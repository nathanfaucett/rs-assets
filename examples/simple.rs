extern crate assets;
extern crate futures;
extern crate futures_cpupool;
extern crate specs;
extern crate specs_time;

use futures_cpupool::CpuPool;

use assets::{
    FileExporter, FileImporter, ImageAsset, ImageAssetExportOptions, ImageAssetImportOptions,
    JSONAsset, Manager,
};
use specs::{DispatcherBuilder, ReadExpect, System, World};
use specs_time::{Time, TimeSystem};

use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::{thread, time};

pub type JSONManager = Manager<JSONAsset, FileImporter<PathBuf>, FileExporter<PathBuf>>;
pub type ImageManager = Manager<ImageAsset, FileImporter<PathBuf>, FileExporter<PathBuf>>;

pub struct SimpleSystem {
    done: Arc<AtomicBool>,
}

impl Default for SimpleSystem {
    fn default() -> Self {
        SimpleSystem {
            done: Arc::new(AtomicBool::new(false)),
        }
    }
}

impl SimpleSystem {
    pub fn new(done: Arc<AtomicBool>) -> Self {
        SimpleSystem { done: done }
    }
}

impl<'system> System<'system> for SimpleSystem {
    type SystemData = (
        ReadExpect<'system, ImageManager>,
        ReadExpect<'system, JSONManager>,
    );

    fn run(&mut self, (image_handler, json_handler): Self::SystemData) {
        println!(
            "Images importing {:?}, imported {:?}",
            image_handler.importing_count(),
            image_handler.imported_count()
        );
        println!(
            "JSONs importing {:?}, imported {:?}",
            json_handler.importing_count(),
            json_handler.imported_count()
        );

        for event in image_handler.poll_events() {
            println!("{:?}", event);
        }
        for event in json_handler.poll_events() {
            println!("{:?}", event);
        }

        if image_handler.importing_count() == 0 && json_handler.importing_count() == 0 {
            self.done.store(true, Ordering::SeqCst);
        }
    }
}

fn main() {
    let pool = Arc::new(CpuPool::new_num_cpus());
    let file_importer = Arc::new(FileImporter::new());
    let file_exporter = Arc::new(FileExporter::new());

    let mut image_handler =
        ImageManager::new(file_importer.clone(), file_exporter.clone(), pool.clone());
    let mut json_handler = JSONManager::new(file_importer, file_exporter, pool);

    let done = Arc::new(AtomicBool::new(false));
    let mut image_handles = Vec::new();
    let mut json_handles = Vec::new();

    let assets = if cfg!(target_os = "android") {
        Path::new("")
    } else {
        Path::new("assets")
    };

    for _ in 0..4 {
        json_handles.push(json_handler.add(
            assets.join("person.json"),
            (),
            (),
            assets.join("person.json"),
            (),
            (),
            true,
        ));
    }
    for _ in 0..4 {
        image_handles.push(image_handler.add(
            assets.join("crate.jpg"),
            (),
            ImageAssetImportOptions::default(),
            assets.join("crate.jpg"),
            (),
            ImageAssetExportOptions::default(),
            true,
        ));
    }
    for _ in 0..4 {
        json_handles.push(json_handler.add(
            assets.join("person.json"),
            (),
            (),
            assets.join("person.json"),
            (),
            (),
            true,
        ));
    }
    for _ in 0..4 {
        image_handles.push(image_handler.add(
            assets.join("crate.jpg"),
            (),
            ImageAssetImportOptions::default(),
            assets.join("crate.jpg"),
            (),
            ImageAssetExportOptions::default(),
            true,
        ));
    }

    let mut world = World::new();

    world.add_resource(image_handler);
    world.add_resource(json_handler);
    world.add_resource(Time::<f32>::default());

    let mut dispatcher = DispatcherBuilder::new()
        .with(TimeSystem::<f32>::new(), "time_system", &[])
        .with(SimpleSystem::new(done.clone()), "simple_system", &[])
        .build();

    while !done.load(Ordering::Relaxed) {
        dispatcher.dispatch(&mut world.res);
        thread::sleep(time::Duration::from_millis(100));
    }

    let time = world.read_resource::<Time<f32>>();
    println!(
        "Total: {}s {}fps {} frames",
        time.current(),
        time.fps(),
        time.frame()
    );
}
