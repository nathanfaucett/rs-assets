use futures::Future;

pub trait Exporter: Send + Sync {
    type Input;
    type Path: Send + Clone;
    type Options: Send + Clone;
    type Error;
    type Future: 'static + Future<Item = (), Error = Self::Error> + Send;

    fn export(&self, Self::Input, Self::Path, Self::Options) -> Self::Future;
}
