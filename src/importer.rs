use futures::Future;

pub trait Importer {
    type Path: Clone;
    type Options: Clone;
    type Output;
    type Error;
    type Future: 'static + Future<Item = Self::Output, Error = Self::Error> + Send;

    fn import(&self, Self::Path, Self::Options) -> Self::Future;
}
