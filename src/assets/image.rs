use imagefmt::{self, bmp, png, read_from, tga, ColFmt, ColType, Error, Image};

use std::fmt;
use std::io::{BufWriter, Cursor};
use std::ops::{Deref, DerefMut};

use super::super::{Asset, Export, Import};

pub struct ImageAsset(Image<u8>);

impl fmt::Debug for ImageAsset {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Deref for ImageAsset {
    type Target = Image<u8>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for ImageAsset {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Image<u8>> for ImageAsset {
    #[inline(always)]
    fn from(image: Image<u8>) -> Self {
        ImageAsset(image)
    }
}

impl Asset for ImageAsset {}

#[derive(Clone)]
pub enum ImageAssetFormat {
    Jpeg,
    Png,
    Bmp,
    Tga,
}

#[derive(Clone)]
pub struct ImageAssetImportOptions {
    pub col_fmt: ColFmt,
}

impl Default for ImageAssetImportOptions {
    #[inline(always)]
    fn default() -> Self {
        ImageAssetImportOptions {
            col_fmt: ColFmt::Auto,
        }
    }
}

impl Import for ImageAsset {
    type Input = Vec<u8>;
    type Output = Self;
    type Error = Error;
    type Options = ImageAssetImportOptions;

    #[inline]
    fn import(bytes: Self::Input, options: Self::Options) -> Result<Self::Output, Self::Error> {
        Ok(read_from(&mut Cursor::new(bytes), options.col_fmt)?.into())
    }
}

#[derive(Clone)]
pub struct ImageAssetExportOptions {
    pub col_type: ColType,
    pub format: ImageAssetFormat,
}

impl Default for ImageAssetExportOptions {
    #[inline(always)]
    fn default() -> Self {
        ImageAssetExportOptions {
            col_type: ColType::Auto,
            format: ImageAssetFormat::Png,
        }
    }
}

type WriteFn =
    fn(&mut BufWriter<Cursor<Vec<u8>>>, usize, usize, ColFmt, &[u8], ColType, Option<usize>)
        -> imagefmt::Result<()>;

impl Export for ImageAsset {
    type Input = Self;
    type Output = Vec<u8>;
    type Error = Error;
    type Options = ImageAssetExportOptions;

    #[inline]
    fn export(input: &Self::Input, options: Self::Options) -> Result<Self::Output, Self::Error> {
        let write_fn: WriteFn = match &options.format {
            // &ImageAssetFormat::Jpeg => jpeg::write,
            &ImageAssetFormat::Png => png::write,
            &ImageAssetFormat::Bmp => bmp::write,
            &ImageAssetFormat::Tga => tga::write,
            _ => return Err(Error::Unsupported("png not supported for write")),
        };
        let mut writer = BufWriter::new(Cursor::new(Vec::new()));

        write_fn(
            &mut writer,
            input.w,
            input.h,
            input.fmt,
            &input.buf,
            options.col_type,
            None,
        )?;

        Ok(writer
            .into_inner()
            .expect("failed to unwrap export BufWriter<Cursor<Vec<u8>>>")
            .into_inner())
    }
}
