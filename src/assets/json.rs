use serde_json::{from_slice, to_vec, Error, Value};

use std::fmt;
use std::ops::{Deref, DerefMut};

use super::super::{Asset, Export, Import};

#[derive(Serialize, Deserialize)]
pub struct JSONAsset(Value);

impl fmt::Debug for JSONAsset {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Deref for JSONAsset {
    type Target = Value;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for JSONAsset {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Asset for JSONAsset {}

impl Import for JSONAsset {
    type Input = Vec<u8>;
    type Output = Self;
    type Error = Error;
    type Options = ();

    #[inline]
    fn import(bytes: Self::Input, _: Self::Options) -> Result<Self::Output, Self::Error> {
        Ok(JSONAsset::from(from_slice(&*bytes)?))
    }
}

impl Export for JSONAsset {
    type Input = Self;
    type Output = Vec<u8>;
    type Error = Error;
    type Options = ();

    #[inline]
    fn export(input: &Self::Input, _: Self::Options) -> Result<Self::Output, Self::Error> {
        to_vec(&*input)
    }
}
