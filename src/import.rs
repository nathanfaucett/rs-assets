pub trait Import {
    type Input;
    type Output;
    type Error;
    type Options: 'static + Send + Sync + Clone;

    fn import(Self::Input, Self::Options) -> Result<Self::Output, Self::Error>;
}
