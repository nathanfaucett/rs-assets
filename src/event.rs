use std::fmt;
use std::sync::Arc;

use super::{Asset, Export, Exporter, Handle, Import, Importer};

pub enum Event<A, L, S>
where
    A: Asset,
    L: Importer<Output = <A as Import>::Input>,
    S: Exporter<Input = <A as Export>::Output>,
{
    Load(Handle<A>),
    Save(Handle<A>),
    Reimport(Handle<A>),
    Remove(Handle<A>, Arc<A>),
    ExportError(Handle<A>, <A as Export>::Error),
    ImportError(Handle<A>, <A as Import>::Error),
    LoadError(Handle<A>, L::Error),
    SaveError(Handle<A>, S::Error),
}

unsafe impl<A, L, S> Send for Event<A, L, S>
where
    A: Asset,
    L: Importer<Output = <A as Import>::Input>,
    S: Exporter<Input = <A as Export>::Output>,
{}
unsafe impl<A, L, S> Sync for Event<A, L, S>
where
    A: Asset,
    L: Importer<Output = <A as Import>::Input>,
    S: Exporter<Input = <A as Export>::Output>,
{}

impl<A, L, S> fmt::Debug for Event<A, L, S>
where
    A: fmt::Debug + Asset,
    <A as Export>::Error: fmt::Debug,
    <A as Import>::Error: fmt::Debug,
    L: Importer<Output = <A as Import>::Input>,
    L::Error: fmt::Debug,
    S: Exporter<Input = <A as Export>::Output>,
    S::Error: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Event::Load(ref handle) => f.debug_tuple("Load").field(handle).finish(),
            &Event::Save(ref handle) => f.debug_tuple("Save").field(handle).finish(),
            &Event::Reimport(ref handle) => f.debug_tuple("Reimport").field(handle).finish(),
            &Event::Remove(ref handle, ref asset) => {
                f.debug_tuple("Remove").field(handle).field(asset).finish()
            }
            &Event::ExportError(ref handle, ref export_error) => f
                .debug_tuple("ExportError")
                .field(&handle)
                .field(export_error)
                .finish(),
            &Event::ImportError(ref handle, ref import_error) => f
                .debug_tuple("ImportError")
                .field(&handle)
                .field(import_error)
                .finish(),
            &Event::LoadError(ref handle, ref load_error) => f
                .debug_tuple("LoadError")
                .field(&handle)
                .field(load_error)
                .finish(),
            &Event::SaveError(ref handle, ref save_error) => f
                .debug_tuple("SaveError")
                .field(&handle)
                .field(save_error)
                .finish(),
        }
    }
}
