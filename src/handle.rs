use std::cmp::Ordering;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

use super::Asset;

pub struct Handle<A>
where
    A: Asset,
{
    id: u32,
    _marker: PhantomData<A>,
}

unsafe impl<A> Send for Handle<A> where A: Asset {}
unsafe impl<A> Sync for Handle<A> where A: Asset {}

impl<A> fmt::Debug for Handle<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Handle({})", self.id)
    }
}

impl<A> fmt::Display for Handle<A>
where
    A: Asset,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.id)
    }
}

impl<A> Hash for Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl<A> PartialOrd for Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl<A> Ord for Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    fn cmp(&self, other: &Self) -> Ordering {
        self.id.cmp(&other.id)
    }
}

impl<A> PartialEq for Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    fn eq(&self, other: &Self) -> bool {
        self.id.eq(&other.id)
    }
}

impl<A> Eq for Handle<A> where A: Asset {}

impl<A> Clone for Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    fn clone(&self) -> Self {
        Handle {
            id: self.id,
            _marker: PhantomData,
        }
    }
}

impl<A> Copy for Handle<A> where A: Asset {}

impl<A> Handle<A>
where
    A: Asset,
{
    #[inline(always)]
    pub(crate) fn new(id: u32) -> Self {
        Handle {
            id: id,
            _marker: PhantomData,
        }
    }

    #[inline(always)]
    pub fn id(&self) -> u32 {
        self.id
    }
}
