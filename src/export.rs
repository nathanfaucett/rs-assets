pub trait Export {
    type Input;
    type Output: Send;
    type Error: Send;
    type Options: 'static + Send + Sync + Clone;

    fn export(&Self::Input, Self::Options) -> Result<Self::Output, Self::Error>;
}
