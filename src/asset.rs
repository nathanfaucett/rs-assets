use super::{Export, Import};

pub trait Asset:
    'static + Sized + Sync + Send + Export<Input = Self> + Import<Output = Self>
{
}
