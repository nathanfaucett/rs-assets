assets
=====
asynchronous asset management 

```rust
extern crate assets;
extern crate futures_cpupool;
extern crate serde_json;

use assets::{Asset, AssetImport, AssetManager, FileLoader};
use futures_cpupool::CpuPool;

use std::sync::Arc;

#[derive(Debug)]
pub struct JSONAsset(serde_json::Value);

impl Asset for JSONAsset {
    type Data = JSONAsset;
}

impl AssetImport for JSONAsset {
    type Input = Vec<u8>;
    type Output = Self;
    type Error = serde_json::Error;
    type Options = ();

    #[inline]
    fn import(bytes: Self::Input, _: Self::Options) -> Result<Self::Output, Self::Error> {
        let data: serde_json::Value = serde_json::from_slice(&*bytes)?;
        Ok(JSONAsset(data))
    }
}

pub type JSONAssetManager<P> = AssetManager<JSONAsset, FileLoader<P>>;

fn main() {
    let pool = Arc::new(CpuPool::new_num_cpus());
    let file_loader = Arc::new(FileLoader::new());

    let mut json_asset_manager = JSONAssetManager::new(file_loader, pool);

    // add asset and starts load with the option preload = true
    let handle = json_asset_manager.add("assets/person.json", (), (), true);

    println!("Total Assets {:?}", json_asset_manager.count());
    println!("Assets Loading {:?}", json_asset_manager.loading_count());
    println!("Assets Loaded {:?}", json_asset_manager.loaded_count());

    // wait for the only event that should be emitted, which is hopefully a Load
    let _load_event = json_asset_manager.wait_event().unwrap();

    // if it loaded this should print the json
    println!("{:?}", json_asset_manager.get(&handle).unwrap());
}
```
